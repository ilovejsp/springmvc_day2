package logic;

public interface Shop {
	
	User getUserByUserIdAndPassword(String userId, String password);
	void entryUser(User user);
}