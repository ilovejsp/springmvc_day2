<%@ page language="java" contentType="text/html; charset=EUC-KR"
	pageEncoding="EUC-KR"%>
<%@ include file="/WEB-INF/jsp/jsp_header.jsp"%>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script src="jquery.blockUI.js"></script>
<html>
<head>
<title>로그인 화면</title>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.0/jquery.min.js"></script>    <script type ="text/javascript"> 
    $(document).ready(function(){ 
        $("#bshow").click(function(){ 
            $("div").show(); 
        }); 
        $("#bhide").click(function(){ 
            $("div").hide(); 
        }); 
    }); 
    </script> 
</head>
<body>
<button id="bshow" >회원가입하기</button>
<button id="bhide" >취소</button>
<div align="center" class="body" style="display: none;">
<h2>로그인 화면</h2>
<form:form modelAttribute="user" method="post" action="login.html">
	<spring:hasBindErrors name="user">
		<font color="red"><c:forEach items="${errors.globalErrors}"
			var="error">
			<spring:message code="${error.code}"  />
		</c:forEach> </font>
	</spring:hasBindErrors>
	<table>
		<tr height="40px">
			<td>유저ID</td>
			<td><form:input path="userId" cssClass="userId" /><font color="red"><form:errors
				path="userId" /></font></td>
		</tr>
		<tr height="40px">
			<td>패스워드</td>
			<td><form:password path="password" cssClass="password" /><font color="red"><form:errors
				path="password" /></font></td>
		</tr>
	</table>
	<table>
		<tr>
			<td align="center"><input type="submit" value="로그인"></td>
			<td align="center"><input type="reset" value="리셋"></td>
		</tr>
	</table>
</form:form></div>

</body>
</html>